import React from 'react';

const NotificationComponent = () => {
  const showNotification = () => {
    if ('Notification' in window) {
      Notification.requestPermission().then(permission => {
        if (permission === 'granted') {
          new Notification('Hello!', {
            body: 'This is a notification from your React app.',
          });
        }
      });
    }
  };

  return (
    <div>
      <button onClick={showNotification}>Show Notification</button>
    </div>
  );
};

export default NotificationComponent;