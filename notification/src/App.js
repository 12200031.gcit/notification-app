import React from 'react';
import NotificationComponent from './component/NotificationComponent.js';

const App = () => {
  return (
    <div>
      <h1>My React App</h1>
      <NotificationComponent />
    </div>
  );
};

export default App;